specials = ".-@_"

def fun(s):
    atsign_idx = s.find("@")
    second_atsign_idx = s.find("@", atsign_idx + 1)
    dot_idx = s.find(".")
    second_dot_idx = s.find(".", dot_idx + 1)
    for i, ch in enumerate(s):
        if not ch.isalpha() and not ch.isdigit():
            if i > atsign_idx:
                if ch in specials and ch != '.':
                    return False
            else:
                if ch not in specials:
                    return False
    return not any([dot_idx == -1, atsign_idx <= 0, second_atsign_idx != -1, second_dot_idx != -1, dot_idx < atsign_idx, len(s) - dot_idx - 1 > 3])

def filter_mail(emails):
    return list(filter(fun, emails))

if __name__ == '__main__':
    n = int(input())
    emails = []
    for _ in range(n):
        emails.append(input())

filtered_emails = filter_mail(emails)
filtered_emails.sort()
print(filtered_emails)