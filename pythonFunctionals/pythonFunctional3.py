# n = int(input())
# nums = []
# dens = []

# for i in range(n):
#     num, den = input().split()
#     nums.append(int(num))
#     dens.append(int(den))

# from functools import reduce
# num = reduce (lambda x, y: x*y, nums)
# den = reduce (lambda x, y: x*y, dens)

# from fractions import gcd
# print(int(num / gcd(num, den)), int(den / gcd(num, den)))


from fractions import Fraction
from functools import reduce

def product(fracs):
    numeratos = [frac.numerator for frac in fracs]
    denominators = [frac.denominator for frac in fracs]
    t = Fraction(reduce(lambda x, y: x * y, numeratos), reduce(lambda x, y: x * y, denominators))
    return t.numerator, t.denominator

if __name__ == '__main__':
    fracs = []
    for _ in range(int(input())):
        fracs.append(Fraction(*map(int, input().split())))
    result = product(fracs)
    print(*result)