cube = lambda x: x**3

def fibonacci(n):
    if n == 0:
        return []
    elif n == 1:
        return [0]
    ret = [0, 1]
    for i in range(2, n):
        ret.append(ret[-1] + ret[-2])
    return ret

if __name__ == '__main__':
    n = int(input())
    print(list(map(cube, fibonacci(n))))