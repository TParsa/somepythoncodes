n = int(input())
a = set(map(int, input().split()))
m = int(input())
b = set(map(int, input().split()))

c = list(a.difference(b).union(b.difference(a)))
c.sort()
for item in c:
    print(item)