import re

n = int(input())
for i in range(n):
    is_valid = False
    regex = input()
    try:
        re.compile(regex)
        is_valid = True
    except:
        is_valid = False
    print(is_valid)