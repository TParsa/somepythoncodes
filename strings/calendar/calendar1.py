import calendar
import datetime

def find_day(date):
    return calendar.day_name[datetime.datetime.strptime(date, "%m %d %Y").weekday()].upper()

date = input()
print(find_day(date))