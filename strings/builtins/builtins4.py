def palindrom(s):
    return s[::-1] == s

n = int(input())
a = input().split()
first_cond = [ x > 0 for x in list(map(int, a)) ]
second_cond = [ palindrom(x) for x in a ]

print( all(first_cond) and any(second_cond) )