n, m = input().split()
n, m = int(n), int(m)

subjects = []

for i in range(m):
    subjects.append(list(map(float, input().split())))

grades = list(zip(*subjects))

for i in range(n):
    print(sum(grades[i]) / len(grades[i]))