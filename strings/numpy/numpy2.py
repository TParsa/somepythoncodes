n, m = input().split()
n, m = int(n), int(m)
a = []
for i in range(n):
    a.append(list(map(int, input().split())))

import numpy as np
print(np.max(np.min(np.array(a), axis=1)))