import copy

def wrapper(f):
    def partition(a, pivot):
        b = []
        c = []
        for num in a:
            if num < pivot:
                b.append(num)
            else:
                c.append(num)
        return b, c

    def quick_sort(a):
        if len(a) <= 1:
            return a 
        idx = 0
        b = []
        c = []
        while len(b) == 0 or len(c) == 0:
            pivot = a[idx]
            b, c = partition(a, pivot)
            idx += 1
        return quick_sort(copy.deepcopy(b)) + quick_sort(copy.deepcopy(c))

    def inner():
        a = f()
        return quick_sort(a)

    return inner

import random

@wrapper
def generate_random_list():
    n = random.randint(10, 21)
    return [random.randint(1, 1000) for i in range(n)]

if __name__ == '__main__':
    print(*generate_random_list(), sep = " ")