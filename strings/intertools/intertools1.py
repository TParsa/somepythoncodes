import itertools
a = list(map(int, input().split()))
b = list(map(int, input().split()))
for element in itertools.product(a, b):
    print(element, end = ' ')
print()