from collections import Counter

n = int(input())
sizes = list(map(int, input().split()))

m = int(input())

buyers = {}
for i in range(m):
    size, price = input().split()
    if(size in buyers):
        buyers[size].append(price)
    else:
        buyers[size] = [price]

availabe = Counter(sizes)

profit = 0

for buyer in buyers:
    profit += sum(list(map(int, buyers[buyer][:availabe[int(buyer)]])))

print(profit)