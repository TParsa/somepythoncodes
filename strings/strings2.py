def count_substring(string, sub_string):
    cnt = 0
    start_idx = 0
    while True:
        idx = string.find(sub_string, start_idx)
        if(idx == -1):
            break
        else:
            cnt += 1
            start_idx = idx + 1
    return cnt

if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()
    
    count = count_substring(string, sub_string)
    print(count)