if __name__ == '__main__':

    n = int(input())
    grades = {}
    for i in range(n):
        name, g1, g2, g3 = input().split()
        grades[name] = [g1, g2, g3]

    name = input()
    sum = 0
    for i in grades[name]:
        sum += float(i)
    print('{:.2f}'.format(sum / len(grades[name])))