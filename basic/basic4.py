n = int(input())
a = []
for i in range(n):
    command = list(input().split())
    if(command[0] == 'insert'):
        if(int(command[1]) == 0):
            a = [int(command[2])] + a
        else:
            a = a[:int(command[1])] + [int(command[2])] + a[int(command[1]):]
    elif(command[0] == 'print'):
        print(a)
    elif(command[0] == 'remove'):
        a.remove(int(command[1]))
    elif(command[0] == 'append'):
        a.append(int(command[1]))
    elif(command[0] == 'sort'):
        a.sort()
    elif(command[0] == 'pop'):
        a = a[:-1]
    else:
        a = a[::-1]