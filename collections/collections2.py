from collections import defaultdict

n, m = input().split()

n, m = int(n), int(m)

d = defaultdict(list)

for i in range(n):
    string = input()
    d[string].append(i + 1)

for i in range(m):
    string = input()
    if(len(d[string])):
        print(*d[string])
    else:
        print (-1)